<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>mail</title>
</head>

<body>
    <div id="errors"></div>
    <form actions="" method="POST" id="mailform">
        <p>Имя:</p>
        <input type="text" name="name" />
        <p>E-mail:</p>
        <input type="email" name="email" />
        <p>Номер телефона:</p>
        <input type="tel" name="phone" />
        <p>Сообщение:</p>
        <textarea name="message"></textarea>
        <input type="submit" name="submit" value="Отправить" />
    </form>
    <script>
        form = document.getElementById('mailform');

        function validate(field) {
            if (field === '') return false;

            return true;
        }

        form.addEventListener('submit', e => {
            e.preventDefault();
            errors = [];
            name = e.target.querySelector('[name="name"]').value;
            email = e.target.querySelector('[name="email"]').value;
            phone = e.target.querySelector('[name="phone"]').value;
            message = e.target.querySelector('[name="message"]').value;

            if (!validate(name)) errors.push('Имя не заполнено');
            if (!validate(email)) errors.push('E-mail не заполнено');

            if (errors.length) {
                div = document.getElementById('errors');

                // Удаляем старые ошибки
                div.querySelectorAll('*').forEach(x => {
                    x.remove();
                });

                errors.forEach(x => {
                    errorDiv = document.createElement('div');

                    errorDiv.innerText = x;

                    div.append(errorDiv);
                })

                return false;
            }

            httpRequest = new XMLHttpRequest();
            httpRequest.open('POST', 'http://localhost:8080/db/3/mail.php', true);
            httpRequest.setRequestHeader("Content-type", "application/x-www-form-urlencoded");

            str = 'name=' + name + '&email=' + email + '&=phone' + phone + '&=message' + message;
            httpRequest.send(str);

        })
    </script>
</body>

</html>