<?php
require_once '../1/db.php';

if ($_POST['submit']) {
    try {
        $uploadDir = $_SERVER['DOCUMENT_ROOT'] . '/uploads';

        if (!file_exists($uploadDir)) mkdir($uploadDir);
        $uploadedFiles = $_FILES['files'];
        validateFiles($uploadedFiles);

        uploadFiles($uploadedFiles, $uploadDir, $conn);
        header('Location: http://localhost:8080/db/2/images.php');
        die();
    } catch (Exception $e) {
        echo $e->getMessage();
    }
} else {
    showForm();
}

function showForm()
{
    $form = '<form method="POST" action="" enctype="multipart/form-data">
<input type="file" name="files[]" multiple>
<input type="submit" name="submit" value="Отправить">
</form>
';
    echo $form;
}

function validateFiles($uploadedFiles)
{
    foreach ($uploadedFiles as $field => $item) {
        foreach ($item as $value) {

            if ($field === 'type' && $value !== 'image/png') throw new Exception('Wrong image type');

            if ($field === 'tmp_name') {
                $fileSizes = getimagesize($value);

                if ($fileSizes[0] < 200 && $fileSizes[1] < 200) throw new Exception('Wrong image size(pixels)');

                if ($filesize = filesize($value) > pow(1024, 2)) throw new Exception("Wrong file size(bytes). $filesize , expected: <1Mb");
            }
        }
    }
}

function uploadFiles($uploadedFiles, $uploadDir, $dbConnection)
{
    foreach ($uploadedFiles as $field => $item) {
        foreach ($item as $value) {

            if ($field === 'name') {
                $extension = pathinfo($value)['extension'];
            }

            if ($field === 'tmp_name') {
                $uniqueName = md5(uniqid(rand(), true));

                move_uploaded_file($value, $uploadDir . "/$uniqueName.$extension");

                $path = $uploadDir . "/$uniqueName.$extension";

                $queryStr = "INSERT INTO uploads (url, date) VALUES ";
                $query = $dbConnection->prepare($queryStr . "('/uploads/$uniqueName.$extension', '" .  time() . "')");
                try {
                    $query->execute();
                } catch (PDOException $e) {
                    echo $e->getMessage();
                }
            }
        }
    }
}
