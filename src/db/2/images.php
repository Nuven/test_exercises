<?php

require_once "../../db/1/db.php";

$queryStr = "SELECT * FROM uploads";
$query = $conn->prepare($queryStr);
$query->execute();

$images = $query->fetchAll();

?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Images</title>
</head>

<body>
    <ul>
        <? foreach($images as $image): ?>
        <li>
            <img src="<?= $image['url']; ?>" alt="" width="400">
        </li>
        <? endforeach; ?>
    </ul>
</body>

</html>