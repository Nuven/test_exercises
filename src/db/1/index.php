<?php
require_once './db.php';

$queryStr = "SELECT * FROM news";

$query = $conn->prepare($queryStr);
$query->execute();

$news = $query->fetchAll();

$perPage = 20;
$currentPage = 1;
$sort = [10, 20, 50, 100, 'all'];

if ($_REQUEST['perPage'] && $_REQUEST['perPage'] !== 'all') {
    $perPage = (int)$_REQUEST['perPage'];
}

if ($_REQUEST['perPage'] === 'all') {
    $perPage = 'all';
}

$currentPage = (int)$_REQUEST['page'] !== 0 ? (int)$_REQUEST['page'] : 1;

$newsChunks = array_chunk($news, $perPage === 'all' ? count($news) : $perPage);
$pages = count($newsChunks);

$currentPage = $currentPage > $pages ? 1 : $currentPage;
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>News</title>
    <style type="text/css">
        a {
            text-decoration: none;
            color: black;
            padding: 2px;
        }

        .active {
            color: green;
            border: 1px solid green;
        }

        .pagination {
            margin-top: 15px;
        }

        .read-more {
            color: blue;
            text-decoration: underline;
        }
    </style>
</head>

<body>
    <div>Количество выводимых новостей:
        <? foreach($sort as $item): ?>
        <a href="?perPage=<?= $item; ?>&page=<?= $currentPage; ?>" class="<?= $perPage === $item ? 'active' : ''; ?>"><?= $item === 'all' ? 'Все' : $item ?></a>
        <? endforeach; ?>
    </div>

    <div class="pagination">
        Страницы:
        <? for($i = 1; $i < $pages+1; $i++): ?>
        <a href="?perPage=<?= $perPage; ?>&page=<?= $i ?>" class="<?= $currentPage === $i ? 'active' : ''; ?>"><?= $i ?></a>
        <? endfor; ?>
    </div>
    <div class=" news">
        <? foreach($newsChunks[$currentPage - 1] as $item): ?>
        <div class="item">
            <h2><?= $item['title'] ?></h2>
            <p><?= $item['description'] ?></p>
            <p><?= $item['text'] ?></p>
            <span>
                <?= date('d.m.Y', $item['date']) ?></span>
            <a class="read-more" href="new.php?title=<?= $item['title']; ?>">Читать полностью...</a>
        </div>
        <? endforeach; ?>
    </div>
</body>

</html>