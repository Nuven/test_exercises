<?php
require_once './db.php';
$title = $_REQUEST['title'];
$queryStr = "SELECT * FROM news WHERE title= '$title'";

$query = $conn->prepare($queryStr);
$query->execute();

$item = $query->fetch();
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>$title</title>
</head>

<body>
    <div class="item">
        <h2><?= $item['title'] ?></h2>
        <p><?= $item['description'] ?></p>
        <p><?= $item['text'] ?></p>
        <span>
            <?= date('d.m.Y', $item['date']) ?></span>
    </div>
</body>

</html>