<?php
require_once __DIR__ . '/../vendor/autoload.php';
require_once './db.php';

use Faker\Factory as Faker;

$news = generateNews();

$queryStr = "INSERT INTO news (title, description, text, date) VALUES ";

$queryStrItems = [];

// Подготавливаем запрос
foreach ($news as $item) {
    $queryStrItems[] = "('" . $item['title'] . "', '" . $item['description'] . "', '" . $item['text'] . "', '" . $item['date'] . "')";
}
$queryStr .= implode(',', $queryStrItems);

try {
    $query = $conn->prepare($queryStr);
    $query->execute();
} catch (PDOException $e) {
    echo $e->getMessage();
}

function generateNews()
{
    $faker = Faker::create();
    $news = [];

    for ($i = 0; $i < 100; $i++) {
        array_push($news, [
            'title' => $faker->text(20),
            'description' => $faker->text(100),
            'text' => $faker->text(),
            'date' => time()
        ]);
    }

    return $news;
}
