<?php
// require_once включает файл, если он не был включен. Require в отличии от include генерирует фатальную ошибку, а не предупреждение. Поскольку предупреждение можно не заметить, лучше использовать require.
require_once('./function.php');

if (function_exists('result')) {
    echo result();
} else {
    echo "Такой функции не существует";
}
