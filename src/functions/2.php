<?php

echo test([1, 2, 3, 4, 5, 6, 7, 8, 9, 10], 'asd');

function test(array $arr, $b, $c = null)
{
    if (count($arr) < 10) {
        echo 'Массив должен содержать больше 10 чисел';
        return;
    }

    return array_sum($arr) / count($arr);
}
