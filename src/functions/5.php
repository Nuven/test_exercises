<?php

$str = 'Ехал Грека через реку, 
Видит Грека - в реке рак. 
Сунул Грека руку в реку, 
Рак за руку Грека - цап!
';

$replaced = preg_replace_callback('/Грека/u', function () {
    return 'Жека';
}, $str);

$result = preg_replace_callback('/[А-Я]/u', function ($arr) {
    return mb_strtolower($arr[0]);
}, $replaced);

echo $result;
