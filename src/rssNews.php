<?php
require_once './db/1/db.php';

$news = file_get_contents('https://habr.com/ru/rss/all/all/');
$newsArray = new SimpleXMLElement($news);
$lastNews = makeChunk($newsArray, 5);

saveDb($lastNews, $conn);

function makeChunk($rss, $size)
{
    $result = [];
    $i = 0;

    foreach ($rss->channel->item as $item) {
        $result[] = $item;
        $i++;
        if ($size === $i) break;
    }
    return $result;
}

function saveDb($arr, $conn)
{
    foreach ($arr as $item) {
        $queryExists = "SELECT * FROM rssNews WHERE title='" . $item->title . "'";
        $query = $conn->prepare($queryExists);
        $query->execute();
        $isExists = count($query->fetchAll());

        // Если записи не существует в бд -- сохраняем
        if (!$isExists) {
            $queryInsert = "INSERT INTO rssNews (title, description, link) VALUES ";
            $queryInsert .= "('" . $item->title . "', '" . $item->description . "', '" . $item->link . "')";

            $query = $conn->prepare($queryInsert);
            $query->execute();
        }
    }
}
?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>RssNews</title>
</head>

<body>
    <div class="news">
        <? foreach($lastNews as $item): ?>
        <div class="item">

            <p class="title"><?= $item->title; ?></p>

            <a href="<?= $item->link ?>" target="_blank">Ссылка на новость</a>

        </div>
        <? endforeach; ?>
    </div>
</body>

</html>