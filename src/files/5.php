<?php
$uploadDir = $_SERVER['DOCUMENT_ROOT'] . '/uploads';

$arr = [
    [
        'title' => 'Игры разума',
        'year' => 2001,
    ],
    [
        'title' => 'Омерзительная восьмерка',
        'year' => 2015,
    ],
    [
        'title' => 'Помни',
        'year' => 2000,
    ],
    [
        'title' => 'Большой Лебовски',
        'year' => 1998,
    ],
    [
        'title' => 'Престиж',
        'year' => 2006,
    ],
];

$xml = new SimpleXMLElement('<xml encoding="UTF-8"/>',);

$list = $xml->addChild('LIST');
foreach ($arr as $film) {
    $item = $list->addChild('ITEM');
    $item->addChild('TITLE', $film['title']);
    $item->addChild('AGE', $film['year']);
}

$xml->saveXML("$uploadDir/films.xml");
