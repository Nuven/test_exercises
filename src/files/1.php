<?php

$form = '
<form method="POST" action="">
    <input type="text" name="filename" placeholder="Название файла">
    <textarea placeholder="Ваше сообщение" name="content"> </textarea>
    <input type="submit" name="submit" value="Отправить">
';

echo $form;

$uploadDir = $_SERVER['DOCUMENT_ROOT'] . '/uploads';

$filename = htmlspecialchars($_POST['filename']);
$content = htmlspecialchars($_POST['content']);

$path = $uploadDir . "/$filename";

if ($_POST['submit'] && !empty($filename)) {
    if (!file_exists($path)) {
        try {
            $file = fopen($path, 'w');
            fwrite($file, $content);
            fclose($file);

            echo "Файл создан";
        } catch (Exception $e) {
            echo var_dump($e);
        }
    } else {
        echo 'Файл с таким именем уже существует';
    }
}
