<?php
$uploadDir = $_SERVER['DOCUMENT_ROOT'] . '/uploads';
$filename = 'data.json';
$path = $uploadDir . "/$filename";

$arr = [
    'Членистоногие' => [
        'Ракообразные',
        'Паукобразные'
    ],
    'Хордовые' => [
        'Рыбы',
        'Млекопитающие' => [
            'Хищные' => [
                'Кошачьи',
                'Псовые' => [
                    'Собака'
                ]
            ],
            'Грызуны' => [
                'Заячьи',
                'Беличьи'
            ]
        ]
    ],
];

$json = json_encode($arr);

if (!file_exists($path)) {
    $file = fopen($path, 'w');
    fwrite($file, $json);
    fclose($file);
}

if (file_exists($path)) {
    $file = fopen($path, 'r');
    $data = fread($file, filesize($path));
    fclose($file);
}

$data = json_decode($data, true);

printTree($data, "-");

function printTree($arr, $delimiter)
{
    foreach ($arr as $key => $value) {

        if (is_string($key)) echo '<br>' . $delimiter . $key;

        if (is_array($value)) printTree($value, $delimiter .= $delimiter[0]);
        else echo '<br>' . $delimiter . $value;
    }
}
