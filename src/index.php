<?php

# Задание 1/1
$name = 'Андрей';
$age = 23;

echo "Меня зовут $name. Мне $age лет.";
echo '<br>';

# Задание 1/2
$p = 37;
$s = $p + 5;
$a = ($p + $s) * 1.5;

echo $a + $s + $p;

# Задание 1/3
define('TEST', 'value', true);
var_dump(defined('Test'));

echo '<br>';

# Задание 1/4
$v1 = '13M';
$v2 = '2G';

$K = 1024;
$M = pow($K, 2);
$G = pow($K, 3);
$T = pow($K, 4);

echo "$v1 в байтах: " . substr($v1, 0, -1) * ${substr($v1, -1, 1)};
echo "<br>";
echo "$v2 в байтах: " . substr($v2, 0, -1) * ${substr($v2, -1, 1)};

echo '<br>';

# Задание 1/5
$day = 3;

$daysArr = [
    1 => ['name' => 'Понедельник', 'type' => 'рабочий день'],
    2 => ['name' => 'Вторник', 'type' => 'рабочий день'],
    3 => ['name' => 'Среда', 'type' => 'рабочий день'],
    4 => ['name' => 'Четверг', 'type' => 'рабочий день'],
    5 => ['name' => 'Пятница', 'type' => 'рабочий день'],
    6 => ['name' => 'Суббота', 'type' => 'укороченный день'],
    7 => ['name' => 'Воскресенье', 'type' => 'выходной день']
];

echo $daysArr[$day]['name'] . ' Это ' . $daysArr[$day]['type'];

echo '<br>';
