<?php
# Задание 1
$arr = [
    'name' => 'Андрей',
    'age' => 23,
    'city' => 'Барнаул'
];

echo implode(',', $arr);

echo '<br>';

foreach ($arr as $key => $value) {
    echo $value;

    if ($key !== array_key_last($arr)) echo ',';
}

# Задание 2
echo '<table border="1">';

echo '<tr>';
for ($x = 1; $x < 10; $x++) {
    echo "<th>$x</th>";
}
echo '</tr>';

echo '<tr>';
for ($x = 1; $x < 10; $x++) {
    echo '<tr>';
    for ($y = 1; $y < 10; $y++) {
        if (($x * $y) % 2 === 0) echo '<td style="background-color: grey;">';
        else echo '<td >';
        echo $x * $y . '</td>';
    }
    echo '</tr>';
}
echo '</tr></table>';


# Задание 3 
$arr = array(
    0 => array(69, -1, 52, 98, 11, 22),
    1 => array(1, -1, 55, 98, 5, 1.5, 8.7),
    2 => array(-15, -9, 10, 0, 5, 3, 0.5),
    3 => array(7, -19, 40, 0, 98, 1, 2.2),
    4 => array('two', 'eight', 'four', 'six')
);

$stringNumbers = [
    'one' => 1,
    'two' => 2,
    'three' => 3,
    'four' => 4,
    'five' => 5,
    'six' => 6,
    'seven' => 7,
    'eight' => 8,
    'nine' => 9,
    'ten' => 10
];

$oneLvlArr = array_unique(array_merge(...$arr));

usort($oneLvlArr, function ($a, $b) use ($stringNumbers) {
    if ($a === $b) {
        return 0;
    }

    if (is_string($a)) $a = $stringNumbers[$a];
    if (is_string($b)) $b = $stringNumbers[$b];

    return ($a > $b) ? 1 : -1;
});

echo "<pre>";

echo print_r($oneLvlArr);

# Задание 4
$arr = array(7, 58, 'Вишня', 1.7, 'Апельсин', 36, 'Картофель', 'Яблоко', 'Киви', 66, 'Капуста');

array_push($arr, 77, 'Банан', 13);

$numbers = array_filter($arr, function ($item) {
    return !is_string($item) && is_int($item);
});

rsort($numbers);

$strings = array_filter($arr, function ($item) {
    return is_string($item);
});

sort($strings);

echo "<pre>";

echo print_r($numbers);
echo print_r($strings);

echo '</pre><br>';

# Задание 5

$arr = [
    0 => [
        'brand' => 'Lada',
        'model' => 'Granta',
        'color' => 'black'
    ],
    1 => [
        'brand' => 'BMW',
        'model' => 'm5',
        'color' => 'blue'
    ],
    2 => [
        'brand' => 'Mercedes',
        'model' => 'gle',
        'color' => 'white'
    ],
];

for ($i = 0; $i < count($arr); $i++) {
    echo "Brand: " . $arr[$i]['brand'] . " Model: " . $arr[$i]['model'] . " Color: " . $arr[$i]['color'];
    echo '<br>';
}

echo '<br>';

foreach ($arr as $key => $value) {
    echo "Brand: " . $value['brand'] . " Model: " . $value['model'] . " Color: " . $value['color'];
    echo '<br>';
}
